# meditrack_backend

Vous aurez besoin de <a href="https://nodejs.org">Node.js</a> installé sur votre ordinateur pour pouvoir créer cette application.

```bash
$ git clone https://github.com/MediTracks/meditrack_backend.git
$ cd meditrack_backend
$ npm install
$ npm start
$ npm install --save-dev nodemon
```