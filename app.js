var Couchbase = require("couchbase");
var Express = require("express");
var UUID = require("uuid");
var BodyParser = require("body-parser");
var BCrypt = require("bcryptjs");

var app = Express();
var N1qlQuery = Couchbase.N1qlQuery;

var request1 = require('request');

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({extended: true}));

var cluster = new Couchbase.Cluster("couchbase://localhost");
//var cluster = new Couchbase.Cluster("couchbase://35.223.156.137");	
// For Couchbase > 4.5 with RBAC Auth
cluster.authenticate('gbisimwa', 'changeme')
var bucket = cluster.openBucket("Cordaid_bucket");

app.post("/zone_d_sante", (request, response) => {
    if(!request.body.nom){
        return response.status(401).send({ "message": "Veiller completer le nom da la zs"});
    } else if(!request.body.addresse){
        return response.status(401).send({ "message": "Veiller completer l'addresse de la zs"});
    } else if(!request.body.contact){
        return response.status(401).send({ "message": "Veiller completer le contact de la zs"});
    }
    
    var zonde_d_sante = {
        "date_creation": (new Date()).getTime(),
        "statut": 0,
        "type": 'zs',
        "nom":request.body.nom,
        "addresse":request.body.addresse,
        "contact":request.body.contact,
    }
    bucket.insert(UUID.v4(), zonde_d_sante, (error, result) => {
        if(error){
            return response.status(500).send(error);
        }
        response.send(zonde_d_sante);
    });
});

app.get("/zone_d_sante", (request, response) => {
    var query = N1qlQuery.fromString("SELECT  META().id, "+bucket._name+".* FROM "+bucket._name+" WHERE type = 'zs'");
    bucket.query(query, (error, result)=>{
        if(error){
            return response.status(500).send(error);
        }
        response.send(result);
    })
});

var server = app.listen(3001, () =>{
    console.log("Listening on port " + server.address().port + " ...");
});
